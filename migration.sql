USE restaurants;

CREATE TABLE recipe (
    recipe_id INT PRIMARY KEY NOT NULL auto_increment,
    name varchar(56)
);

CREATE TABLE product_recipe (
    product_id INT,
    recipe_id INT,
    quantity INT,
    PRIMARY KEY (product_id, recipe_id),
    FOREIGN KEY (product_id) REFERENCES product (product_id),
    FOREIGN KEY (recipe_id) REFERENCES recipe (recipe_id)
);

grant all privileges on *.* to 'david'@'localhost';