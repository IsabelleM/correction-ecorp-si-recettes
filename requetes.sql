USE restaurants;

#Connaître l'état des stocks dans chaque restaurant ainsi que le stock global (cumulé)
SELECT restaurant.name, quantity, SUM(stock.quantity) FROM stock
JOIN restaurant ON stock.restaurant_id = restaurant.restaurant_id
JOIN product ON product.product_id = stock.product_id
WHERE product.name = 'pain'
GROUP BY restaurant.name, stock.quantity;

#Insérer un nouveau produit
INSERT INTO product (name) VALUES (NULL, "eau");

#Pouvoir augmenter ou diminuer le stock d’un produit dans un restaurant donné
UPDATE stock
SET quantity = quantity + 1000
WHERE product_id = 1 AND restaurant_id = 1;

#Retrouver une recette grâce à son nom
SELECT recipe.name FROM recipe;


#Ajouter une recette (la liste de ses ingrédients et les quantités)
INSERT INTO recipe VALUES (NULL, "crêpes");

INSERT INTO product_recipe (recipe_id,product_id,quantity) VALUES 
(6, (SELECT product_id FROM product WHERE name = "farine"), 100),
(6, (SELECT product_id FROM product WHERE name = "oeuf"), 3),
(6, (SELECT product_id FROM product WHERE name = "sucre"), 100),
(6, (SELECT product_id FROM product WHERE name = "lait"), 100),
(6, (SELECT product_id FROM product WHERE name = "dopamine"), 1);

#Pouvoir savoir combien de fois je peux faire une recette donnée dans chacun des restaurants.