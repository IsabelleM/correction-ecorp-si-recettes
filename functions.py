# Mettre tout en minuscule
# Si il y a un S à la fin
#     je l'enleve
# enlever les ligature
# enlever les espaces
# enlever les accents
# enlever les ", ' + symbole louche
# retourner le resultat

def normalisation(name):
    #Mettre tout en minuscule
    name = name.lower()
    #enlever le s à la fin
    if name.endswith('s'):
        name = name[:-1]
    #enlever les espaces
    name = name.replace("'", "\'")
    name = name.replace(" ","")
    #enlever les accents
    name = name.replace("é", "e")
    #Supprimer les ligatures
    name = name.replace('œ', 'oe').replace('æ', 'ae')
    #enlever les symboles louches
    name = ''.join(c for c in name if c.isalnum())
    return name



# Function pour normalizer
def normalizer(chaine):
    # Mettre tout en minuscule
    chaine = chaine.lower()

    # Enlever un 'S' à la fin, si présent
    if chaine.endswith('s'):
        chaine = chaine[:-1]

    # Enlever les ligatures
    ligatures = {'æ': 'ae', 'œ': 'oe'}
    for ligature, replacement in ligatures.items():
        chaine = chaine.replace(ligature, replacement)

    # Enlever les espaces
    chaine = chaine.replace(' ', '')

    # Enlever les accents
    accents = {'é': 'e', 'è': 'e', 'ê': 'e', 'à': 'a', 'â': 'a', 'ù': 'u', 'û': 'u', 'î': 'i', 'ô': 'o'}
    for accent, replacement in accents.items():
        chaine = chaine.replace(accent, replacement)

    # Enlever les ", ', et autres symboles spéciaux
    chaine = ''.join(c for c in chaine if c.isalnum() or c in ['-', '_'])

    return chaine