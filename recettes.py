import mysql.connector
from unidecode import unidecode

# connect à mysql
connection = mysql.connector.connect (
    host="localhost",       #l'url de la base de données
    user="david",           #le nom d'utilisateur mysql
    password="coucou123",   #le mot de passe de l'utilisateur mysql
    database="restaurants"  #le nom de la base de données
)
curse = connection.cursor(dictionary=True)

def normalize(l):
    return unidecode(" ".join([t[:-1] if t.endswith("s") else t for t in l.lower().split(' ')]))


# ouvre le fichier recette.csv
with open('/home/isabelle/ecorp_si_recettes/recettes.csv') as csv:
# pour chaque ligne
    for line in csv.readlines():
        entre = line.split(",")  
        # recuperer le nom de la recette
        nom_recette = normalize(entre[2]).replace('"', '').strip()
        print(nom_recette)

        # recup la qt de l'ingredient
        try: 
            qt = int(entre[1])
        except:
            continue
        
        if qt < 0:
            qt = 0

        # → si elle dans la bdd, je recup id
        curse.execute('select recipe_id from recipe where name like %s', [nom_recette])
        if result := curse.fetchone():
            id_recette = result["recipe_id"]
            #   sinon je l'ajoute
        else:
            curse.execute('insert into recipe(name) values (%s)', [nom_recette])
            id_recette = curse.lastrowid
        
        
        # recuperer le nom de l'ingredient
        nom_ingredient = normalize(entre[0]).replace('"', '').strip()
        # id de l'ingredient
        curse.execute('select product_id from product where name like %s', [nom_ingredient])
        if result := curse.fetchone():
            id_prod = result["product_id"]
        else:
            curse.execute('insert into product(name) values (%s)', [nom_ingredient])
            id_prod = curse.lastrowid

        # → ajouter dans la table ingredient id_recette_ id _ingredient _ qt
        curse.execute('insert into product_recipe values (%s, %s, %s)', [id_prod, id_recette, qt])



# commit de la bdd !
connection.commit()


# Fermer le curseur et la connexion
curse.close()
connection.close()
