import mysql.connector
from unidecode import unidecode
from fastapi import FastAPI, HTTPException

# connect à mysql
connection = mysql.connector.connect (
    host="localhost",       #l'url de la base de données
    user="david",           #le nom d'utilisateur mysql
    password="coucou123",   #le mot de passe de l'utilisateur mysql
    database="restaurants"  #le nom de la base de données
)
curse = connection.cursor(dictionary=True)

def normalize(l):
    return unidecode(" ".join([t[:-1] if t.endswith("s") else t for t in l.lower().split(' ')]))

app=FastAPI()

@app.get("/product/{name}")
def stock_produit_resto(name):
    curse.execute("""SELECT restaurant.name, quantity, SUM(stock.quantity) FROM stock
        JOIN restaurant ON stock.restaurant_id = restaurant.restaurant_id
        JOIN product ON product.product_id = stock.product_id
        WHERE product.name = %s
        GROUP BY restaurant.name, stock.quantity;""", [name])
    return curse.fetchall()


@app.post("/product/{name}")
def ajouter_produit(name):
    curse.execute(""" INSERT INTO product (name) VALUES (%s);""", [name])
    connection.commit()
    return curse.fetchall()

@app.get("/recipe/{name}")
def retrouver_recette(name):
    curse.execute("""SELECT recipe.name FROM recipe
                  WHERE recipe.name = %s;""" [name])
    connection.commit()
    return curse.fetchone()

@app.put("/recipe/{name}")
def add_recipe(name):
    curse.execute("""INSERT INTO recipe VALUES (NULL, %s);
        """, [name])
    connection.commit()
    return curse.fetchall()

@app.put("/stock/{quantity}/{product_id}/{restaurant_id}")
def augmenter_quantite(quantity: int, product_id: int, restaurant_id: int):
    curse.execute("""UPDATE stock
        SET quantity = quantity + %s
        WHERE product_id = %s AND restaurant_id = %s;""", (quantity,product_id,restaurant_id))
    connection.commit()
    return curse.fetchall()

