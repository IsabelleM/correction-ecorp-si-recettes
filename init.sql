DROP DATABASE IF EXISTS restaurants;

CREATE DATABASE IF NOT EXISTS restaurants;

USE restaurants;

CREATE TABLE product (
    product_id INT NOT NULL PRIMARY KEY auto_increment,
    name VARCHAR (56)
    UNIQUE (name)
);

CREATE TABLE restaurant (
    restaurant_id INT NOT NULL PRIMARY KEY auto_increment,
    name VARCHAR(56),
    UNIQUE (name)
);

CREATE TABLE stock (
    product_id INT,
    restaurant_id INT,
    quantity INT,
    PRIMARY KEY (product_id, restaurant_id),
    FOREIGN KEY (product_id) REFERENCES product (product_id),
    FOREIGN KEY (restaurant_id) REFERENCES restaurant (restaurant_id)
);

grant all privileges on *.* to 'david'@'localhost';