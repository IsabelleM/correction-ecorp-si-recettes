import mysql.connector
from functions import normalisation
from unidecode import unidecode

# connect à mysql
connection = mysql.connector.connect(
    host="localhost",       #l'url de la base de données
    user="david",           #le nom d'utilisateur mysql
    password="coucou123",   #le mot de passe de l'utilisateur mysql
    database="restaurants"  #le nom de la base de données
)


cursor = connection.cursor(dictionary=True)

# Créer le resto A
try:
    cursor.execute('insert into restaurant(name) values ("A");')
    restaurant_A_id = cursor.lastrowid
except:
    cursor.execute('select restaurant_id from restaurant where name like "A";')
    restaurant_A_id = cursor.fetchone()["restaurant_id"]
# recup l'id du resto

def normalize(l):
    return unidecode(" ".join([t[:-1] if t.endswith("s") else t for t in l.lower().split(' ')]))

# il est ou A.csv
# Ouvrir le fichier a.csv
with open("/home/isabelle/ecorp_si_recettes/extract_A.csv", "r") as A_csv:
# lire une ligne du fichier
    for line in A_csv.readlines():
        prod = line.split(",")
# recup le nom du produit
        nom_produit = prod[0]
# recup la qt + cast en int
        try: # ici on fait un try, comme ca si la qt c'est autre chose qu'un nombre on saute la ligne
            quantity = int(prod[1])
        except:
            continue # pour passer directement à l'itération suivante de la boucle
# verifier que c'est un int > 0
        if quantity < 0:
            quantity = 0
# normaliser le nom
        nom_produit = normalisation(nom_produit)

        cursor.execute('select product_id from product where name like %s', [nom_produit])
        if prod := cursor.fetchone():
            # il est déjà dedans !
            # récuperer l'id du produit qui existe déjà
            id_prod = prod["product_id"]
            # recuperer la qt qu'il y a déjà dans la bdd
            cursor.execute("select quantity from stock where product_id = %s and restaurant_id = %s;", [id_prod, restaurant_A_id])
            stock_bdd = cursor.fetchone()
            # ajouter la qt de la ligne
            quantity += stock_bdd["quantity"]
            # mettre à jour la qt dans la bdd
            cursor.execute('update stock set quantity = %s where product_id = %s and restaurant_id = %s;', [quantity, id_prod, restaurant_A_id])
        else:
            # ajouter le produit à la bdd
            cursor.execute('insert into product(name) values (%s)', [nom_produit])

            # recup l'id du produit
            id_prod = cursor.lastrowid

            # ajouter la qt au resto A
            cursor.execute('insert into stock values (%s, %s, %s)', [id_prod, restaurant_A_id, quantity])



# Valider les changements
connection.commit()

# Fermer le curseur et la connexion
cursor.close()
connection.close()